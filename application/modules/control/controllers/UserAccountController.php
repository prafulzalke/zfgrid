<?php 

class Control_UserAccountController extends Zend_Controller_Action 
{
    public function indexAction()
    {
        $field = $this->_getParam('sort', 'id');
        $order = $this->_getParam('order', 'ASC');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('users')->order($field . ' ' . $order);

        $grid = new ZF_Grid();
        $grid->setSelect($select);
        $grid->addExtraColumn(['url' => $this->view->url() . '/edit?id={id}', 'class' => 'teal edit']);
        $grid->addExtraColumn(['url' => $this->view->url(), 'class' => 'teal trash']);
        $grid->addExtraColumn(['title' => 'online', 'url' => '', 'class' => 'green signal']);
        $this->view->grid = $grid;

        //$this->view->sort = ['field' => $field, 'order' => $order];
    }
    
   
}
