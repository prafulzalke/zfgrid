<?php
/**
 * Control module bootstrap
 *
 * @package control
 */
class Control_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * Init namespace
     */
    public function _initNamespaces()
    {
        echo APPLICATION_PATH; die;
        $resourceLoader = new Zend_Loader_Autoloader_Resource([
            'basePath' => APPLICATION_PATH . '/modules/bpt',
            'namespace' => 'Bpt',
            'resourceTypes' => [
                'service' => [
                    'path' => 'services/',
                    'namespace' => 'Service',
                ],
            ],
        ]);

    }
}