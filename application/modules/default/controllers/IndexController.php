<?php 

class IndexController extends Zend_Controller_Action 
{
    public function indexAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('users')->order('id');
        $paginator = Zend_Paginator::factory($select);
        $paginator->setDefaultItemCountPerPage(5);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));

        $this->view->paginator = $paginator;
    }
    
    public function postAction()
    {

    }
}