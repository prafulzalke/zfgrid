<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setName("login");
        $this->setMethod('post');
             
        $username = new Zend_Form_Element_Text("username");
        $username->setAttrib('style', 'width:300px;font-size:20px;height:10px;')
            ->setRequired(true)
            ->removeDecorator('label')
            ->setLabel('Username')
            ->setAttrib('placeholder', 'Enter your email')
            ->setAttrib('autocomplete', 'off')
            ->removeDecorator('HtmlTag');
        
        $password = new Zend_Form_Element_Password("password");
        $password->setAttrib('style', 'width:300px;font-size:20px;height:10px;')
            ->setRequired(true)
            ->removeDecorator('label')
            ->setLabel('Password')
            ->setAttrib('placeholder', 'password')
            ->removeDecorator('HtmlTag');

        $this->addElements(array($username,$password));
    }


}

