<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap 
{

    protected function _initConfig() 
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set("config", $config);
    }

    protected function _initTranslate() 
    {
        
    }

    protected function _initLogmailer()
    {
        $logmailerConfig = $this->getOption('logmailer');

        $mailer = new Zend_Mail();
        $transport = new Zend_Mail_Transport_Smtp(
            $logmailerConfig['smtp']['server'],
            $logmailerConfig['smtp']
        );

        $mailer->setDefaultTransport($transport);
        Zend_Registry::set('logmailer', $mailer);
    }

    protected function _initNameSpace()
    {
        $resourceLoader = new Zend_Loader_Autoloader_Resource([
            'basePath' => APPLICATION_PATH . '/modules/control',
            'namespace' => 'Control',
            'resourceTypes' => [
                'service' => [
                    'path' => 'services/',
                    'namespace' => 'Service',
                ],
            ],
        ]);

        return $resourceLoader;
    }
}