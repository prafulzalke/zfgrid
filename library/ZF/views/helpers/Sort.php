<?php

/**
 */
class ZF_View_Helper_Sort extends Zend_View_Helper_Abstract
{
    public function sort($field, $filter)
    {
        if ($field == $filter['field'] && strtoupper($filter['order'] == 'ASC')) {
            $order = 'DESC';
        } else {
            $order = 'ASC';
        }

        $href = sprintf('%s', $this->view->url() . '?sort=' . $field . '&order=' . $order);

        return $href;
    }
}