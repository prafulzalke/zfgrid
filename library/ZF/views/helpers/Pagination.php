<?php

/**
 */
class ZF_View_Helper_Pagination extends Zend_View_Helper_Abstract
{
    public function pagination($paginator, $params)
    {
        $template = '<div class="ui right floated pagination menu">
                        <a class="icon item" {{firstLink}}>
                            <i class="left chevron icon"></i>
                        </a>
                        {{otherLink}}
                        <a class="icon item" {{lastLink}}>
                            <i class="right chevron icon"></i>
                        </a>
                    </div>';

        $filter = [];
        
        foreach ($params as $k => $v) {
            $filter[] = $k . '=' . $v;
        }
        
        if (count($filter) > 0) {
            $filter = implode('&', $filter) . '&';
        }
        
        $href = $this->view->url() . '?' . $filter . 'page=';

        foreach ($paginator->getPages()->pagesInRange as $pageId) {

            $text = $pageId;
            $url = $href . $pageId;

            if ($paginator->getCurrentPageNumber() == $pageId) {
                $text = sprintf('<h4>%d</h4>', $pageId);
            }

            $otherLink[] = sprintf('<a class="item" href="%s">%s</a>', $url, $text);
        }

        if ($paginator->getPages()->current == $paginator->getPages()->first) {
            $data['{{firstLink}}'] = '';
        } else {
            $data['{{firstLink}}'] = 'href="' . $href . $paginator->getPages()->previous . '"';;
        }

        $data['{{lastLink}}'] = 'href="' . $href . $paginator->getPages()->next . '"';
        $data['{{otherLink}}'] = implode(PHP_EOL, $otherLink);

        echo strtr($template, $data);
       
    }
}