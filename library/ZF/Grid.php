<?php

/**
 * Class for php grid using zend
 *
 */
class ZF_Grid
{
    const DEFAULT_ITEM_PER_PAGE = 5;
    const DEFAULT_PAGE_RANGE = 5;

    /**
     * @var Zend_View
     */
    protected $_view;

    /**
     * @var Zend_Db_Select
     */
    protected $_select;

    /**
     * @var int
     */
    protected $_page;

    /**
     * @var array
     */
    protected $_links = [];

    /**
     * @var array
     */
    protected $_columns = [];

    /**
     * @return Zend_view
     */
    public function getView()
    {
        if (null === $this->_view) {
            $this->_view = new Zend_View();
            $this->_view->addHelperPath(__DIR__ . DIRECTORY_SEPARATOR . 'views/helpers', 'ZF_View_Helper_');
            $this->_view->setScriptPath(__DIR__ . DIRECTORY_SEPARATOR . 'views/scripts/template');
        }

        return $this->_view;
    }

    public function getSelect()
    {
        return $this->_select;
    }

    public function setSelect($select)
    {
        $this->_select = $select;
    }

    public function getPage()
    {
        $controller = Zend_Controller_Front::getInstance();
        $page = $controller->getRequest()->getParam('page', 1);
        $this->getView()->assign('page', $page);
        return $page;
    }

    public function getPaginator()
    {
        $paginator = Zend_Paginator::factory($this->getSelect());
        $paginator->setDefaultItemCountPerPage(self::DEFAULT_ITEM_PER_PAGE);
        $paginator->setDefaultPageRange(self::DEFAULT_PAGE_RANGE);
        $paginator->setCurrentPageNumber($this->getPage());

        return $paginator;
    }

    public function setLinks($links)
    {
        if (is_array($links)) {
            $this->_links = $links;
        } else {
            throw new InvalidArgumentException('setLinks function accept only array');
        }
    }

    public function getLinks()
    {

    }

    public function addExtraColumn($column)
    {
        if (is_array($column)) {
            $this->_columns[] = $column;
        } else {
            throw new InvalidArgumentException('addExtraColumn function accept only array');
        }
    }

    public function getExtraColumns()
    {
        return $this->_columns;
    }
    
    public function getFilters()
    {
        $controller = Zend_Controller_Front::getInstance();
        $field = $controller->getRequest()->getParam('sort', 'id');
        $order = $controller->getRequest()->getParam('order', 'ASC');
        $params = $controller->getRequest()->getParams();
        unset($params['module'], $params['controller'], $params['action']);
        return $params;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        $this->getView()->assign('params', $this->getFilters());
        $this->getView()->assign('paginator', $this->getPaginator());
        $this->getView()->assign('extraColumns', $this->getExtraColumns());
        return $this->getView()->render('grid.phtml');
    }
}